package ru.nsu.ccfit.g16207.voytenko.theater.entities;


import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "zvaniya")
public class Zvanie implements BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    @Column(name = "name")
    private String name;

    public Zvanie(String name){
        this.name = name;
    }

    @Override
    public void setText(String text) {
        this.name = text;
    }
}
