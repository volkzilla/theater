package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Department;
import ru.nsu.ccfit.g16207.voytenko.theater.util.HibernateSessionFactoryUtil;

import java.util.List;

public class DepartmentDao extends BaseDao<Department> {
    public DepartmentDao() {
        super(Department.class);
    }

    public String getData(){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        String tmp = "Department";
        String data = ((Department) session.createQuery(String.format("FROM %s where id = '12'", tmp)).uniqueResult()).getName();
        session.close();
        return data;
    }

    public Department findByName(String name){
        return (Department) HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(String.format("FROM %s where name = %s", "departments", name)).getSingleResult();
    }
}
