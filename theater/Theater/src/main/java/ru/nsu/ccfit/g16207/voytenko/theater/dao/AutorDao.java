package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Autor;

public class AutorDao extends BaseDao<Autor> {
    public AutorDao() {
        super(Autor.class);
    }
}
