package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "departments")
public class Department implements BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    @Column(name = "name")
    private String name;

    public Department(String name){
        this.name = name;
    }

    public Department() { }

    public String getName() {
        return name;
    }

    @Override
    public void setText(String text) {
        this.name = text;
    }

    public int getId() {
        return id;
    }
}
