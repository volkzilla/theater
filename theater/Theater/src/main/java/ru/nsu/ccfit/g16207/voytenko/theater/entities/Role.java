package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role implements BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String name;

    @Override
    public void setText(String text) {
        this.name = text;
    }

    public Role() { }
}
