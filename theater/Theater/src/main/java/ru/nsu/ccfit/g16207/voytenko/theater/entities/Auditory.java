package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "auditories")
public class Auditory implements BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String name;

    public Auditory(String name){
        this.name = name;
    }

    public Auditory() { }

    @Override
    public void setText(String text) {
        this.name = text;
    }
}
