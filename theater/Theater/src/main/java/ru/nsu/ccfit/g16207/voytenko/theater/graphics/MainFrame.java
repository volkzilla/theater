package ru.nsu.ccfit.g16207.voytenko.theater.graphics;

import ru.nsu.ccfit.g16207.voytenko.theater.common.ActionType;
import ru.nsu.ccfit.g16207.voytenko.theater.common.Constants;
import ru.nsu.ccfit.g16207.voytenko.theater.common.Tools;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.*;
import ru.nsu.ccfit.g16207.voytenko.theater.services.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainFrame extends JFrame {


    private DepartmentService departmentService = Tools.departmentService;
    private PossitionService possitionService = Tools.possitionService;
    private ZvanieService zvanieService = Tools.zvanieService;
    private GanreService ganreService = new GanreService();
    private RoleService roleService = new RoleService();

    private static final String APP_TITLE = "THEATER SYSTEM";
    private JMenuBar menuBar = new JMenuBar();
    private StatusBar statusBar = new StatusBar();

    private MainPanel mainPanel;
    private JScrollPane scrollPane;
    private ArrayList<JComponent> activeComponents = new ArrayList<>();
    private HashMap<IconType, ImageIcon> icons = new HashMap<>();

    public MainFrame(){
        setTitle(APP_TITLE);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });

        setSize(new Dimension(700, 600));
        setJMenuBar(menuBar);

        JMenu showMenu = createMenuItem("Show", KeyEvent.VK_S);
        JMenu addMenu = createMenuItem("Add", KeyEvent.VK_E);
        JMenu deleteMenu = createMenuItem("Delete", KeyEvent.VK_D);
        JMenu updateMenu = createMenuItem("Update", KeyEvent.VK_U);
        JMenu findMenu = createMenuItem("Find", KeyEvent.VK_F);
        JMenu helpMenu = createMenuItem("Help", KeyEvent.VK_H);

        //prepareIcons();

        JMenuItem addPossitionMenuItem = createSubMenuItem(addMenu, Constants.POSSITION_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление позиции", this::showAddPossitionDialog);
        JMenuItem addRoleMenuItem = createSubMenuItem(addMenu, Constants.ROLE_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление роли", this::showAddRoleDialog);
        JMenuItem addGenreMenuItem = createSubMenuItem(addMenu, Constants.GENRE_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление жанра", this::showAddGenreDialog);
        JMenuItem addDepartmentMenuItem = createSubMenuItem(addMenu, Constants.DEPARTMENT_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление департамента", this::showAddDepartmentDialog);
        JMenuItem addZvanieMenuItem = createSubMenuItem(addMenu, Constants.ZVANIE_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление звания", this::showAddZvanieDialog);
        //JMenuItem addVidanoeZvanieMenuItem = createSubMenuItem(addMenu, "Выданное звание", KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление выданного звания", this::addToTableAction);
        //JMenuItem addWorkerMenuItem = createSubMenuItem(addMenu, "Работник", KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление работника", this::addToTableAction);

        JMenuItem deletePossitionMenuItem = createSubMenuItem(deleteMenu, "Позиция", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление позиции", this::showDeletePossitionDialog);
        JMenuItem deleteRoleMenuItem = createSubMenuItem(deleteMenu, "Роль", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление роли", this::showDeleteRoleDialog);
        JMenuItem deleteGenreMenuItem = createSubMenuItem(deleteMenu, "Жанр", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление жанра", this::showDeleteGanreDialog);
        JMenuItem deleteDepartmentMenuItem = createSubMenuItem(deleteMenu, "Департамент", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление департамента", this::showDeleteDepartmentDialog);
        JMenuItem deleteZvanieMenuItem = createSubMenuItem(deleteMenu, "Звание", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление звания", this::showDeleteZvanieDialog);
        JMenuItem deleteVidanoeZvanieMenuItem = createSubMenuItem(deleteMenu, "Выданное звание", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление выданного звания", this::deleteFromTableAction);
        JMenuItem deleteWorkerMenuItem = createSubMenuItem(deleteMenu, "Работник", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление работника", this::deleteFromTableAction);

        JMenuItem showDepartmentMenuItem = createSubMenuItem(showMenu, "Департамент", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать департаменты", this::showLookDepartments);


        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);

        mainPanel = new MainPanel();
        scrollPane = new JScrollPane(mainPanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(5);
        scrollPane.setBackground(Color.WHITE);
        add(scrollPane);
        GridBagConstraints gbc1 = new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0);
        gbl.addLayoutComponent(scrollPane, gbc1);


        //TextField textField = new TextField();
        //textField.setSize(new Dimension(400, 300));
//        GridBagConstraints tgbc = new GridBagConstraints();
//        tgbc.anchor = GridBagConstraints.NORTHWEST;
//        add(textField);
//        gbl.addLayoutComponent(textField, tgbc);

        add(statusBar);
        Insets sBarInsets = new Insets(0, 0, 0, 0);
        GridBagConstraints sBarConstraints = new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, sBarInsets, 0, 0);
        gbl.addLayoutComponent(statusBar, sBarConstraints);

        setVisible(true);
    }


    private JMenu createMenuItem(String name, int mnemonic){
        JMenu menu = new JMenu(name);
        menu.setMnemonic(mnemonic);
        menuBar.add(menu);
        return menu;
    }

    private JMenuItem createSubMenuItem(JMenu menu, String name, int mnemonic, ImageIcon icon, String text, Runnable event){
        JMenuItem item = new JMenuItem(name, icon);
        item.setMnemonic(mnemonic);
        menu.add(item);
        statusBar.addHint(text, item);

        item.addActionListener(l -> {
            statusBar.showHint(null);
            if (event != null){
                event.run();
            }
        });

        item.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.showHint(item);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.showHint(null);
            }
        });
        activeComponents.add(item);
        item.setBorder(BorderFactory.createEmptyBorder());
        return item;
    }

    //*****************ДОБАВЛЕНИЕ*************************
    private void showAddDepartmentDialog(){
        new OneValueDialog(this, new Department("base"), departmentService, ActionType.SAVE, Constants.DEPARTMENT_TEXT);
    }

    private void showAddPossitionDialog(){
        new OneValueDialog(this, new Possition("base"), possitionService,ActionType.SAVE, Constants.POSSITION_TEXT);
    }

    private void showAddZvanieDialog(){
        new OneValueDialog(this, new Zvanie("base"), zvanieService, ActionType.SAVE,Constants.ZVANIE_TEXT);
    }

    private void showAddGenreDialog(){
        new OneValueDialog(this, new Ganre("base"), ganreService, ActionType.SAVE,Constants.GENRE_TEXT);
    }

    private void showAddRoleDialog(){
        new OneValueDialog(this, new Role(), roleService, ActionType.SAVE, Constants.ROLE_TEXT);
    }


    //*****************УДАЛЕНИЕ***************************
    private void showDeleteDepartmentDialog(){
        new OneValueDialog(this, null, departmentService, ActionType.DELETE, Constants.DEPARTMENT_TEXT);
    }

    private void showDeletePossitionDialog(){
        new OneValueDialog(this, null, possitionService, ActionType.DELETE, Constants.POSSITION_TEXT);
    }

    private void showDeleteZvanieDialog(){
        new OneValueDialog(this, new Department(), zvanieService, ActionType.DELETE, Constants.ZVANIE_TEXT);
    }

    private void showDeleteGanreDialog(){
        new OneValueDialog(this, new Department(), ganreService, ActionType.DELETE, Constants.GENRE_TEXT);
    }

    private void showDeleteRoleDialog(){
        new OneValueDialog(this, new Department(), roleService, ActionType.DELETE, Constants.ROLE_TEXT);
    }


    //*******************ПРОСМОТР***********************
    private void showLookDepartments(){
        StringBuilder stringBuilder = new StringBuilder();
        List<Department> departments = Tools.allDepartments();
        for (Department tmp : departments){
            stringBuilder.append("id = " + tmp.getId() + " name = " + tmp.getName());
            stringBuilder.append("\n");
        }
        JOptionPane.showMessageDialog(this, stringBuilder.toString());
    }

    private void addToTableAction() {
        JOptionPane.showMessageDialog(this, "Операция выполнена, данные добавлены", "Добавление",
                JOptionPane.PLAIN_MESSAGE);
    }

    private void deleteFromTableAction() {
        JOptionPane.showMessageDialog(this, "Операция выполнена, данные удалены", "Удаление",
                JOptionPane.PLAIN_MESSAGE);
    }

}
