package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.AbonementDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Abonement;

public class AbonementService implements IService<Abonement> {
    private AbonementDao abonementDao = new AbonementDao();

    @Override
    public void add(Abonement abonement) {
        abonementDao.save(abonement);
    }

    @Override
    public void delete(Abonement abonement) {
        abonementDao.delete(abonement);
    }

    @Override
    public void edit(Abonement abonement) {
        abonementDao.update(abonement);
    }

    @Override
    public Abonement findById(int id) {
        return abonementDao.findById(id);
    }
}
