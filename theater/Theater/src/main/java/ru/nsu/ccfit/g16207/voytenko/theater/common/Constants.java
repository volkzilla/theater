package ru.nsu.ccfit.g16207.voytenko.theater.common;

public class Constants {

    public static final String POSSITION_TEXT = "Позиция";
    public static final String ZVANIE_TEXT = "Звание";
    public static final String GENRE_TEXT = "Жанр";
    public static final String DEPARTMENT_TEXT = "Департамент";
    public static final String ROLE_TEXT = "Роль";

}
