package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Repertuar;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Spectacl;
import ru.nsu.ccfit.g16207.voytenko.theater.util.HibernateSessionFactoryUtil;

public class RepertuarDao extends BaseDao<Repertuar> {
    public RepertuarDao() {
        super(Repertuar.class);
    }

}
