package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Abonement;

public class AbonementDao extends BaseDao<Abonement> {
    public AbonementDao() {
        super(Abonement.class);
    }

}
