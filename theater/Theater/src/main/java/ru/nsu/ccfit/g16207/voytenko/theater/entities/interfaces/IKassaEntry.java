package ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces;

public interface IKassaEntry extends BaseEntity {
    void setPrice(int price);
    void setTicketsCount(int m);
    void setMesto(String mesto);
}
