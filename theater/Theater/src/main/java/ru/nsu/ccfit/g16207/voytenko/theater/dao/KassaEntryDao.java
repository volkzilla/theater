package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.KassaEntry;

public class KassaEntryDao extends BaseDao<KassaEntry> {
    public KassaEntryDao() {
        super(KassaEntry.class);
    }
}
