package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Possition;
import ru.nsu.ccfit.g16207.voytenko.theater.dao.PossitionDao;

public class PossitionService implements IService<Possition>{

    private PossitionDao possitionDao = new PossitionDao();

    public PossitionService(){
    }

    public void add(Possition possition){
        possitionDao.save(possition);
    }

    public void delete(Possition possition){
        possitionDao.delete(possition);
    }

    public void edit(Possition possition){
        possitionDao.update(possition);
    }

    public Possition findById(int workerId){
        return possitionDao.findById(workerId);
    }
}
