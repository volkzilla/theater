package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.AuditoryDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Auditory;

public class AuditoryService implements IService<Auditory> {
    private AuditoryDao auditoryDao = new AuditoryDao();

    @Override
    public void add(Auditory auditory) {
        auditoryDao.save(auditory);
    }

    @Override
    public void delete(Auditory auditory) {
        auditoryDao.delete(auditory);
    }

    @Override
    public void edit(Auditory auditory) {
        auditoryDao.update(auditory);
    }

    @Override
    public Auditory findById(int id) {
        return auditoryDao.findById(id);
    }
}
