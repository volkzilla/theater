package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Zvanie;

public class ZvanieDao extends BaseDao<Zvanie> {
    public ZvanieDao() {
        super(Zvanie.class);
    }
}
