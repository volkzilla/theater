package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import javax.persistence.*;

@Entity
@Table(name = "workers")
public class                                                                                            Worker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;
    private boolean sex;
    private String birthdate;
    private int salary;
    private String offerdate;
    private int possitionId;
    private int departmentId;
    private int zvanieId;
    private int childrens;

    public Worker(boolean sex, String birthdate, int salary, String offerdate, int possitionId,
                  int departmentId, int zvanieId, int childrenCount){
        this.sex = sex;
        this.birthdate = birthdate;
        this.salary = salary;
        this.offerdate = offerdate;
        this.possitionId = possitionId;
        this.departmentId = departmentId;
        this.zvanieId = zvanieId;
        this.childrens = childrenCount;
    }

    public Worker() { }

    public int getId() {
        return id;
    }

    public boolean getSex(){
        return sex;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public int getSalary() {
        return salary;
    }

    public String getOfferdate() {
        return offerdate;
    }

    public int getPossitionId() {
        return possitionId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public int getZvanieId() {
        return zvanieId;
    }
}
