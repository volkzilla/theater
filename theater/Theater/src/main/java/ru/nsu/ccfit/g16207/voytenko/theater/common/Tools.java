package ru.nsu.ccfit.g16207.voytenko.theater.common;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Department;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Possition;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Worker;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Zvanie;
import ru.nsu.ccfit.g16207.voytenko.theater.services.DepartmentService;
import ru.nsu.ccfit.g16207.voytenko.theater.services.PossitionService;
import ru.nsu.ccfit.g16207.voytenko.theater.services.WorkerService;
import ru.nsu.ccfit.g16207.voytenko.theater.services.ZvanieService;

import java.util.List;

public class Tools {
    public static ZvanieService zvanieService = new ZvanieService();
    public static PossitionService possitionService = new PossitionService();
    public static DepartmentService departmentService = new DepartmentService();

    private static WorkerService workerService = new WorkerService();

    public static void initializeDataBase(){
        //initializeZvania();
//        initializePossitions();
//        initializeDepartments();
//        initializeVidanieZvania();
//        initializeWorkers();

        //System.out.println(getDepartments());
    }

    public static void initializeWorkers(){

        Worker worker1 = new Worker(true, "1995-02-21", 25000, "1995-02-25", 1, 16, 1, 3);
        Worker worker2 = new Worker(true, "2005-01-24", 21000, "2008-02-25", 1, 16, 1, 2);
        Worker worker3 = new Worker(false, "2000-09-11", 45000, "2018-03-29", 1, 16, 1, 3);
        Worker worker4 = new Worker(false, "1991-01-10", 18000, "1997-07-27", 1, 16, 1, 1);

        workerService.add(worker1);
        workerService.add(worker2);
        workerService.add(worker3);
        workerService.add(worker4);
    }

    private static void initializeDepartments(){

        Department bosses = new Department("team lead");

        departmentService.add(bosses);

    }

    private static void initializePossitions(){

        Possition managerPossition = new Possition("Manager");

        possitionService.add(managerPossition);

    }

    private static void initializeVidanieZvania(){

    }

    private static void initializeZvania(){

        Zvanie director = new Zvanie("director");
        Zvanie clerk = new Zvanie("clerk");
        Zvanie teacher = new Zvanie("teacher");
        zvanieService.add(teacher);
        zvanieService.add(director);
        zvanieService.add(clerk);
    }

    public static void findFuckingDepartmentAndDelete(int id){
        departmentService.delete(departmentService.findById(id));
    }

    public static List<Department> findAllDepartments(){
        return departmentService.findAll();
    }

    private static String getDepartments(){
        return departmentService.getDepartments();
    }

    public static List<Department> allDepartments(){
        return departmentService.findAll();
    }

    public static List<Worker> allWorkersByChildrenCount(int count){
        return workerService.findWorkersByChildrenCount(count);
    }

    public static List<Worker> allWorkersByBirthYear(int count){
        return workerService.findWorkersByBirthYear(count);
    }

}
