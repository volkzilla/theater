package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "spectacls")
public class Spectacl implements BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String name;

    public Spectacl() { }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "genre_id")
    private Ganre ganre;

    @ManyToMany(mappedBy = "name",fetch = FetchType.LAZY)
    private List<Auditory> auditory;

    @ManyToMany(mappedBy = "fio",fetch = FetchType.LAZY)
    private List<Autor> autor;

    @Override
    public void setText(String text) {
        this.name = text;
    }
}
