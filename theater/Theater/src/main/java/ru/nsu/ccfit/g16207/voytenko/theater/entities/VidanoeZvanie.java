package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import javax.persistence.*;

@Entity
@Table(name = "vidaniezvania")
public class VidanoeZvanie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "worker_id")
    private Worker worker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "zvanie_id")
    private Zvanie zvanie;

    public VidanoeZvanie(){
    }
}
