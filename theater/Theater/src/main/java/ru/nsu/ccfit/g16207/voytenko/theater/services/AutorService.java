package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.AutorDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Autor;

public class AutorService implements IService<Autor> {
    private AutorDao autorDao = new AutorDao();

    @Override
    public void add(Autor autor) {
        autorDao.save(autor);
    }

    @Override
    public void delete(Autor autor) {
        autorDao.delete(autor);
    }

    @Override
    public void edit(Autor autor) {
        autorDao.update(autor);
    }

    @Override
    public Autor findById(int id) {
        return autorDao.findById(id);
    }
}
