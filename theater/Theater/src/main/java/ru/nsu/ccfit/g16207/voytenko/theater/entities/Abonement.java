package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "aubonements")
public class Abonement implements BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "genre_id")
    private Ganre ganre;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fio")
    private Autor autor;

    public Abonement() { }

    @Override
    public void setText(String text) {
        this.date = text;
    }
}
