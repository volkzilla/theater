package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import javax.persistence.*;

@Entity
@Table(name = "autors")
public class Autor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String fio;

    private String country;

    private int vek;

    public Autor(String fio, String country, int vek){
        this.fio = fio;
        this.country = country;
        this.vek = vek;
    }

    public Autor() { }
}
