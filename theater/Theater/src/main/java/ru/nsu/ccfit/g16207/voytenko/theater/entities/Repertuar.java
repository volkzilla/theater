package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import javax.persistence.*;

@Entity
@Table(name = "repertuars")
public class Repertuar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String premiera;

    private String time;

    public Repertuar(String premiera, String time){
        this.premiera = premiera;
        this.time = time;
    }

    public Repertuar() { }
}
