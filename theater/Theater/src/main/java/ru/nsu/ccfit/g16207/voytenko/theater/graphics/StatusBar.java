package ru.nsu.ccfit.g16207.voytenko.theater.graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StatusBar extends JPanel{

    private Map<JComponent, String> hints = new HashMap<>();
    private JLabel hintLabel = new JLabel();
    private JLabel capLabel = new JLabel();
    private JLabel numLabel = new JLabel();
    private JLabel scrlLabel = new JLabel();


    public StatusBar() {
        initialize();
    }

    public void addHint(String text, JComponent obj){
        if (!hints.containsKey(obj)){
            hints.put(obj, text);
        }
    }

    public void showHint(Object obj){
        if (obj != null){
            hintLabel.setText(hints.get(obj));
        } else {
            hintLabel.setText("Ready");
        }
    }

    private void initialize(){
        capLabel.setText("CAP");
        numLabel.setText("NUM");
        scrlLabel.setText("SCRL");
        hintLabel.setText("Ready");

        GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);

        // Hint area
        add(hintLabel);
        gridBagLayout.addLayoutComponent(hintLabel, new GridBagConstraints(0, 0, 1,
                1, 1.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
                new Insets(0, 5, 0, 0), 0, 0));

        GridBagConstraints constraints = new GridBagConstraints(GridBagConstraints.RELATIVE, 0, 1,
                1, 0.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.NONE,
                new Insets(0, 0, 0,5), 0, 0);

        //CAP
        add(capLabel);
        gridBagLayout.addLayoutComponent(capLabel, constraints);
        //NUM
        add(numLabel);
        gridBagLayout.addLayoutComponent(numLabel, constraints);
        //SCR
        add(scrlLabel);
        gridBagLayout.addLayoutComponent(scrlLabel, constraints);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        capLabel.setEnabled(toolkit.getLockingKeyState(KeyEvent.VK_CAPS_LOCK));
        numLabel.setEnabled(toolkit.getLockingKeyState(KeyEvent.VK_NUM_LOCK));
        scrlLabel.setEnabled(toolkit.getLockingKeyState(KeyEvent.VK_SCROLL_LOCK));

        g.clearRect(0, 0, getWidth(), getHeight());
        repaint();
    }

}
