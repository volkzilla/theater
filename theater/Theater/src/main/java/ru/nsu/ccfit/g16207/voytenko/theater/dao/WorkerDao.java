package ru.nsu.ccfit.g16207.voytenko.theater.dao;


import ru.nsu.ccfit.g16207.voytenko.theater.entities.Worker;
import ru.nsu.ccfit.g16207.voytenko.theater.util.HibernateSessionFactoryUtil;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class WorkerDao extends BaseDao<Worker>{

    public WorkerDao(){
        super(Worker.class);
    }

    public List findWorkersByChildrenCount(int childrenCount){
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createNativeQuery("select * From workers WHERE childrens =" + String.valueOf(childrenCount))
                .getResultList();
    }

    public List findWorkersByDepartment(int depId){
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createNativeQuery("select * From workers WHERE departmentid =" + String.valueOf(depId))
                .getResultList();
    }

    public List findWorkersBySalary(int salary){
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createNativeQuery("select * From workers WHERE salary >=" + String.valueOf(salary))
                .getResultList();
    }

    public List findWorkersByOfferDate(Date date){
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createNativeQuery("select * From workers WHERE offerdate >=" + date.toString())
                .getResultList();
    }

    public List findWorkersBySex(boolean sex){
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createNativeQuery("select * From workers WHERE sex =" + String.valueOf(sex))
                .getResultList();
    }

    public List<Worker> findWorkersByBirthYear(int year){
        return findAll().stream().filter(w -> Integer.parseInt(w.getBirthdate().substring(0, 4)) > year).collect(Collectors.toList());
    }


}
