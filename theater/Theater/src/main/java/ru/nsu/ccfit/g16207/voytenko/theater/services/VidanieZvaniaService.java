package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.VidanoeZvanieDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.VidanoeZvanie;

public class VidanieZvaniaService implements IService<VidanoeZvanie> {

    private VidanoeZvanieDao dao = new VidanoeZvanieDao();

    @Override
    public void add(VidanoeZvanie vidanoeZvanie) {
        dao.save(vidanoeZvanie);
    }

    @Override
    public void delete(VidanoeZvanie vidanoeZvanie) {
        dao.delete(vidanoeZvanie);
    }

    @Override
    public void edit(VidanoeZvanie vidanoeZvanie) {
        dao.update(vidanoeZvanie);
    }

    @Override
    public VidanoeZvanie findById(int id) {
        return dao.findById(id);
    }
}
