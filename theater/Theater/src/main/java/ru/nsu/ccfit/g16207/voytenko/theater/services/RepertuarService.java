package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.RepertuarDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Repertuar;

public class RepertuarService implements IService<Repertuar> {
    private RepertuarDao dao = new RepertuarDao();

    @Override
    public void add(Repertuar repertuar) {
        dao.save(repertuar);
    }

    @Override
    public void delete(Repertuar repertuar) {
        dao.delete(repertuar);
    }

    @Override
    public void edit(Repertuar repertuar) {
        dao.update(repertuar);
    }

    @Override
    public Repertuar findById(int id) {
        return dao.findById(id);
    }
}
