package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Auditory;

public class AuditoryDao extends BaseDao<Auditory> {
    public AuditoryDao() {
        super(Auditory.class);
    }
}
