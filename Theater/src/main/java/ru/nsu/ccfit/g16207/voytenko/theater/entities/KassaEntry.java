package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.IKassaEntry;

import javax.persistence.*;

@Entity
@Table(name = "kassa")
public class KassaEntry implements IKassaEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "possitionid")
    private int id = 1;

    private String mesto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "repertuar_id")
    private Repertuar repertuar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "abonement_id")
    private Abonement abonement;

    private int ticketsCount;

    private int price;

    private String date;

    public KassaEntry() { }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public void setTicketsCount(int m) {
        this.ticketsCount = m;
    }

    @Override
    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    @Override
    public void setText(String text) {
        this.date = text;
    }
}
