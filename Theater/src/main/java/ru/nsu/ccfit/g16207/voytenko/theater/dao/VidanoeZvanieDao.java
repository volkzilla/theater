package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.VidanoeZvanie;

public class VidanoeZvanieDao extends BaseDao<VidanoeZvanie>{
    public VidanoeZvanieDao() {
        super(VidanoeZvanie.class);
    }
}
