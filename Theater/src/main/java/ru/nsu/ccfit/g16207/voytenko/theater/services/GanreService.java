package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.GanreDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Ganre;

import java.util.List;

public class GanreService implements IService<Ganre> {
    private GanreDao ganreDao = new GanreDao();

    @Override
    public void add(Ganre ganre) {
        ganreDao.save(ganre);
    }

    @Override
    public void delete(Ganre ganre) {
        ganreDao.delete(ganre);
    }

    @Override
    public void edit(Ganre ganre) {
        ganreDao.update(ganre);
    }

    @Override
    public Ganre findById(int id) {
        return ganreDao.findById(id);
    }

    @Override
    public List<Ganre> findAll() {
        return ganreDao.findAll();
    }
}
