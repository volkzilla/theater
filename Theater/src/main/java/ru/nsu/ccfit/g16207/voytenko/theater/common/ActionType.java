package ru.nsu.ccfit.g16207.voytenko.theater.common;

public enum ActionType {
    SAVE, DELETE, UPDATE, SELECT
}
