package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.nsu.ccfit.g16207.voytenko.theater.util.HibernateSessionFactoryUtil;

import java.util.List;

public abstract class BaseDao<T>{

    private Class<T> typeClass;

    public BaseDao(Class<T> typeClass){
        this.typeClass = typeClass;
    }

    public T findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(typeClass, id);
    }

    public void save(T param){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(param);
        tx1.commit();
        session.close();
    }

    public void update(T param){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(param);
        tx1.commit();
        session.close();
    }

    public void delete(T param){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(param);
        tx1.commit();
        session.close();
    }

    public List<T> findAll(){
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("FROM " + typeClass.getSimpleName(), typeClass).list();
    }



}
