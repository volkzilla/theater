package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "autors")
public class Autor implements BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String fio;

    private String country;

    private int vek;

    public Autor(String fio, String country, int vek){
        this.fio = fio;
        this.country = country;
        this.vek = vek;
    }

    public int getId() {
        return id;
    }

    public int getVek() {
        return vek;
    }

    public String getCountry() {
        return country;
    }

    public String getFio() {
        return fio;
    }

    public Autor() { }

    @Override
    public void setText(String text) {
        this.country = text;
    }
}
