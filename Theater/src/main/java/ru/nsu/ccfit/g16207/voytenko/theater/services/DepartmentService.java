package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Department;
import ru.nsu.ccfit.g16207.voytenko.theater.dao.DepartmentDao;

import java.util.List;

public class DepartmentService implements IService<Department>{
    private DepartmentDao departmentDao = new DepartmentDao();

    public DepartmentService(){
    }

    public String getDepartments(){
        return departmentDao.getData();
    }

    public void add(Department department){
        departmentDao.save(department);
    }

    public void delete(Department department){
        departmentDao.delete(department);
    }

    public void edit(Department department){
        departmentDao.update(department);
    }

    public Department findById(int id){
        return departmentDao.findById(id);
    }

    public Department findByName(String name){
        return departmentDao.findByName(name);
    }

    public List<Department> findAll(){
        return departmentDao.findAll();
    }
}
