package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Worker;
import ru.nsu.ccfit.g16207.voytenko.theater.dao.WorkerDao;

import java.util.List;

public class WorkerService implements IService<Worker>{

    private WorkerDao workerDao = new WorkerDao();

    public WorkerService(){
    }

    public void add(Worker worker){
        workerDao.save(worker);
    }

    public void delete(Worker worker){
        workerDao.delete(worker);
    }

    public void edit(Worker worker){
        workerDao.update(worker);
    }

    public Worker findById(int workerId){
        return workerDao.findById(workerId);
    }

    @Override
    public List<Worker> findAll() {
        return workerDao.findAll();
    }

    public void deleteAll(){
        workerDao.deleteAll();
    }

    public List<Worker> findWorkersByChildrenCount(int childrenCount){
       return workerDao.findWorkersByChildrenCount(childrenCount);
    }

    public List<Worker> findWorkersByBirthYear(int year){
        return workerDao.findWorkersByBirthYear(year);
    }

    public List<Worker> findWorkersByDepartment(String department){
        return workerDao.findWorkersByDepartment(department);
    }

    public List<Worker> findWorkersBySalary(int salary){
        return workerDao.findWorkersBySalary(salary);
    }

    public List<Worker> findWorkersBySex(boolean sex){
        return workerDao.findWorkersBySex(sex);
    }


}
