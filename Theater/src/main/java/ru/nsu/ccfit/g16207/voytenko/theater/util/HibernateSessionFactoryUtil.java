package ru.nsu.ccfit.g16207.voytenko.theater.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.*;

public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(Worker.class);
                configuration.addAnnotatedClass(Possition.class);
                configuration.addAnnotatedClass(Zvanie.class);
                configuration.addAnnotatedClass(Department.class);
                configuration.addAnnotatedClass(Role.class);
                configuration.addAnnotatedClass(Ganre.class);
                configuration.addAnnotatedClass(Abonement.class);
                configuration.addAnnotatedClass(Auditory.class);
                configuration.addAnnotatedClass(Autor.class);
                configuration.addAnnotatedClass(KassaEntry.class);
                configuration.addAnnotatedClass(Repertuar.class);
                configuration.addAnnotatedClass(Spectacl.class);
                configuration.addAnnotatedClass(VidanoeZvanie.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            } catch (Exception e) {
                System.out.println("Исключение!" + e);
            }
        }
        return sessionFactory;
    }
}
