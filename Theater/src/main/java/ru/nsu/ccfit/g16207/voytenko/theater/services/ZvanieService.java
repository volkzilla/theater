package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Zvanie;
import ru.nsu.ccfit.g16207.voytenko.theater.dao.ZvanieDao;

import java.util.List;

public class ZvanieService implements IService<Zvanie> {
    private ZvanieDao zvanieDao = new ZvanieDao();

    @Override
    public void add(Zvanie zvanie) {
        zvanieDao.save(zvanie);
    }

    @Override
    public void delete(Zvanie zvanie) {
        zvanieDao.delete(zvanie);
    }

    @Override
    public void edit(Zvanie zvanie) {
        zvanieDao.update(zvanie);
    }

    @Override
    public Zvanie findById(int id) {
        return zvanieDao.findById(id);
    }

    @Override
    public List<Zvanie> findAll() {
        return zvanieDao.findAll();
    }
}
