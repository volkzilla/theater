package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.IdNameEntity;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role implements BaseEntity, IdNameEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String name;

    @Override
    public void setText(String text) {
        this.name = text;
    }


    public Role(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }


    public Role() { }
}
