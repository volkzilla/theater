package ru.nsu.ccfit.g16207.voytenko.theater.graphics;


import ru.nsu.ccfit.g16207.voytenko.theater.common.ActionType;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;
import ru.nsu.ccfit.g16207.voytenko.theater.services.IService;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowEvent;

public class OneValueDialog extends JDialog {

    private JFrame owner;
    private String storedText;
    private boolean isChanged = false;
    private IService service;
    private BaseEntity entity;
    private ActionType type;

    public OneValueDialog(JFrame owner, BaseEntity entity, IService service, ActionType type, String title){
        this.owner = owner;
        this.entity = entity;
        this.service =service;
        this.type = type;

        //size
        Dimension dimension = new Dimension(400, 250);
        setSize(dimension);

        //layout
        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);

        JPanel inputDataPanel = createPanel("Введите данные");
        add(inputDataPanel);
        Insets insets = new Insets(0, 1, 0, 0);
        gbl.addLayoutComponent(inputDataPanel, new GridBagConstraints(0, 0, 4, 2, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0, 0));

        putConfigurationItem(inputDataPanel, title);

        //OK Button
        JButton okButton = new JButton("OK");
        okButton.addActionListener(l -> okEvent());
        add(okButton);
        Insets insetsOkButton = new Insets(15, 15, 10, 15);
        GridBagConstraints gbc = new GridBagConstraints(1, 2, 1, 1, 1, 0.5, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetsOkButton, 0, 0);
        gbl.addLayoutComponent(okButton, gbc);

//        //INFO Button
//        JButton infoButton = new JButton("INFO");
//        okButton.addActionListener(l -> infoEvent());
//        add(infoButton);
//        GridBagConstraints gbc1 = new GridBagConstraints();
//        gbc1.anchor = GridBagConstraints.SOUTHEAST;
//        gbl.addLayoutComponent(infoButton, gbc1);


        setVisible(true);
    }

    private JPanel createPanel(String panelName){
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        panel.setBorder(new TitledBorder(panelName));
        panel.setFont(Font.getFont("ITALIC"));
        return panel;
    }

    private void putConfigurationItem(JPanel owner, String itemName) {
        GridBagLayout gbl = (GridBagLayout) owner.getLayout();
        Insets insets = new Insets(0, 1, 4, 0);
        GridBagConstraints gbc = new GridBagConstraints(0, GridBagConstraints.RELATIVE, 1, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0, 0);
        JLabel nameLabel = new JLabel(itemName);
        owner.add(nameLabel);
        gbl.addLayoutComponent(nameLabel, gbc);

        JTextField textValue = new JTextField();
        textValue.setSize(new Dimension(100, 20));
        textValue.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offset, String str, AttributeSet set) throws BadLocationException {
                if (str.length() + getLength() > 40) {
                    return;
                }
                super.insertString(offset, str, set);
            }
        });
        textValue.setText("...");
        owner.add(textValue);
        gbc.gridx += 2;
        gbl.addLayoutComponent(textValue, gbc);

        //linking
        Runnable changeEvent = () -> {
            String value;
            try{
                value = textValue.getText();
            } catch (Exception ex){
                textValue.setText("...");
                return;
            }
            storedText = value;
            textValue.setText(value);
            System.out.println("input text = " + storedText);
        };

        textValue.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                changeEvent.run();
            }
        });

        textValue.addActionListener(l -> {
            changeEvent.run();
            update(textValue.getText());
        });
    }

    private void okEvent(){
        switch (type){
            case SAVE:
                entity.setText(storedText);
                service.add(entity);
                break;
            case DELETE:
                if (service.findById(Integer.parseInt(storedText)) == null){
                    JOptionPane.showMessageDialog(this, "ERR: entity with this ID doesn't exist");
                    return;
                }
                service.delete(service.findById(Integer.parseInt(storedText)));
                break;
            case UPDATE:
                entity.setText(storedText);
                service.edit(entity);
                break;
            case SELECT:
                break;
            default:
                break;
        }
        processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    private void update(String text){
        storedText = text;
    }

    public boolean notChanged() {
        return !isChanged;
    }

    public String getStoredText() {
        return storedText;
    }
}
