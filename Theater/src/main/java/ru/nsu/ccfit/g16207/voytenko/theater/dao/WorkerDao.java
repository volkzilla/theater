package ru.nsu.ccfit.g16207.voytenko.theater.dao;


import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Worker;
import ru.nsu.ccfit.g16207.voytenko.theater.util.HibernateSessionFactoryUtil;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class WorkerDao extends BaseDao<Worker>{

    public WorkerDao(){
        super(Worker.class);
    }

    public void deleteAll(){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Worker> workers = session.createQuery("FROM " + Worker.class.getSimpleName()).list();
        for (Worker tmp : workers){
            Transaction tx = session.beginTransaction();
            session.delete(tmp);
            tx.commit();
        }
        session.close();
    }

    public List findWorkersByChildrenCount(int childrenCount){
        return findAll().stream().filter(w -> w.getChildrens() == childrenCount).collect(Collectors.toList());
    }

    public List findWorkersByDepartment(String dep){
        return findAll().stream().filter(w -> w.getDepartment().getName().trim().equals(dep)).collect(Collectors.toList());
    }

    public List findWorkersBySalary(int salary){
        return findAll().stream().filter(w -> w.getSalary() >= salary).collect(Collectors.toList());
    }

    public List findWorkersByOfferDate(Date date){
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createNativeQuery("select * From workers WHERE offerdate >=" + date.toString())
                .getResultList();
    }

    public List findWorkersBySex(boolean sex){
        return findAll().stream().filter(w -> w.getSex() == sex).collect(Collectors.toList());
    }

    public List<Worker> findWorkersByBirthYear(int year){
        return findAll().stream().filter(w -> Integer.parseInt(w.getBirthdate().substring(0, 4)) > year).collect(Collectors.toList());
    }


}
