package ru.nsu.ccfit.g16207.voytenko.theater.common;

public class Constants {

    public static final String POSSITION_TEXT = "Позиция";
    public static final String ZVANIE_TEXT = "Звание";
    public static final String GENRE_TEXT = "Жанр";
    public static final String DEPARTMENT_TEXT = "Департамент";
    public static final String ROLE_TEXT = "Роль";
    public static final String WORKERS_TEXT = "Работники";
    public static final String AUDITORIES_TEXT = "Аудитория";
    public static final String AUTOR_TEXT = "Аудитория";


    public static final String HELP_TEXT = "Приветствую, друг!\n" +
            "Данное приложение является помощником для управления системы театра. \n" +
            "Здесь выделены основные сущности и роли в театре:\n" +
            "          *Департаменты\n" +
            "          *Позиции\n" +
            "          *Роли\n" +
            "          *Звания\n" +
            "          *И тд\n\n" +
            "В меню вы найдете следующие операции:\n" +
            "          *Добавление всего представленного в театре\n" +
            "          *Удаление\n" +
            "          *Редактирование\n" +
            "          *Просмотр по различным критериям\n\n" +
            "Если остались вопросы - пишите v.voitenko@g.nsu.ru";


}
