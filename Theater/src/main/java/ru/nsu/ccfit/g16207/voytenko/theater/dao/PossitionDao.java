package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Possition;

public class PossitionDao extends BaseDao<Possition> {
    public PossitionDao() {
        super(Possition.class);
    }
}
