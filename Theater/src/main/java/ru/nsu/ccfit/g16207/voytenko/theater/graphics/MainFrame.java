package ru.nsu.ccfit.g16207.voytenko.theater.graphics;

import ru.nsu.ccfit.g16207.voytenko.theater.common.ActionType;
import ru.nsu.ccfit.g16207.voytenko.theater.common.Constants;
import ru.nsu.ccfit.g16207.voytenko.theater.common.Tools;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.*;
import ru.nsu.ccfit.g16207.voytenko.theater.services.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainFrame extends JFrame {

    private DepartmentService departmentService = Tools.departmentService;
    private PossitionService possitionService = Tools.possitionService;
    private ZvanieService zvanieService = Tools.zvanieService;
    private GanreService ganreService = Tools.ganreService;
    private RoleService roleService = Tools.roleService;
    private AuditoryService auditoryService = Tools.auditoryService;
    private WorkerService workerService = Tools.workerService;
    private AutorService autorService = Tools.autorService;

    private static final String APP_TITLE = "THEATER SYSTEM";
    private JMenuBar menuBar = new JMenuBar();
    private StatusBar statusBar = new StatusBar();

    private MainPanel mainPanel;
    private JScrollPane scrollPane;
    private ArrayList<JComponent> activeComponents = new ArrayList<>();
    private HashMap<IconType, ImageIcon> icons = new HashMap<>();

    public MainFrame(){
        setTitle(APP_TITLE);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });

        setSize(new Dimension(700, 600));
        setJMenuBar(menuBar);
        ImageIcon imageIcon = new ImageIcon("/home/vladislav/IdeaProjects/Theater/src/main/resources/icon.jpg");
        setIconImage(imageIcon.getImage());
        Image background = null;
        try {
            background = ImageIO.read(new File("/home/vladislav/IdeaProjects/Theater/src/main/resources/icon.jpg"));
        } catch (IOException ignored) { }


        JMenu showMenu = createMenuItem("Show", KeyEvent.VK_S);
        JMenu addMenu = createMenuItem("Add", KeyEvent.VK_E);
        JMenu deleteMenu = createMenuItem("Delete", KeyEvent.VK_D);
        JMenu updateMenu = createMenuItem("Update", KeyEvent.VK_U);
        JMenu helpMenu = createMenuItem("Help", KeyEvent.VK_H);


        JMenuItem addPossitionMenuItem = createSubMenuItem(addMenu, Constants.POSSITION_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление позиции", this::showAddPossitionDialog);
        JMenuItem addRoleMenuItem = createSubMenuItem(addMenu, Constants.ROLE_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление роли", this::showAddRoleDialog);
        JMenuItem addGenreMenuItem = createSubMenuItem(addMenu, Constants.GENRE_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление жанра", this::showAddGenreDialog);
        JMenuItem addDepartmentMenuItem = createSubMenuItem(addMenu, Constants.DEPARTMENT_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление департамента", this::showAddDepartmentDialog);
        JMenuItem addZvanieMenuItem = createSubMenuItem(addMenu, Constants.ZVANIE_TEXT, KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление звания", this::showAddZvanieDialog);
        JMenuItem addWorkerMenuItem = createSubMenuItem(addMenu, "Работник", KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление работника", this::showAddWorkerDialog);
        JMenuItem addAutorMenuItem = createSubMenuItem(addMenu, "Автор", KeyEvent.VK_A, icons.get(IconType.ADD), "Добавление автора", this::showAddAutorDialog);


        JMenuItem deletePossitionMenuItem = createSubMenuItem(deleteMenu, "Позиция", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление позиции", this::showDeletePossitionDialog);
        JMenuItem deleteRoleMenuItem = createSubMenuItem(deleteMenu, "Роль", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление роли", this::showDeleteRoleDialog);
        JMenuItem deleteGenreMenuItem = createSubMenuItem(deleteMenu, "Жанр", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление жанра", this::showDeleteGanreDialog);
        JMenuItem deleteDepartmentMenuItem = createSubMenuItem(deleteMenu, "Департамент", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление департамента", this::showDeleteDepartmentDialog);
        JMenuItem deleteZvanieMenuItem = createSubMenuItem(deleteMenu, "Звание", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление звания", this::showDeleteZvanieDialog);
        JMenuItem deleteVidanoeZvanieMenuItem = createSubMenuItem(deleteMenu, "Выданное звание", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление выданного звания", null);
        JMenuItem deleteWorkerMenuItem = createSubMenuItem(deleteMenu, "Работник", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление работника", this::showDeleteWorkerDialog);
        JMenuItem deleteAutorMenuItem = createSubMenuItem(deleteMenu, "Автор", KeyEvent.VK_A, icons.get(IconType.DELETE), "Удаление автора", this::showDeleteAutorDialog);


        JMenuItem updateGanreMenuItem = createSubMenuItem(updateMenu, "Жанр", KeyEvent.VK_A, icons.get(IconType.DELETE), "Обновление жанра", this::showUpdateGanre);
        JMenuItem updatePossitionMenuItem = createSubMenuItem(updateMenu, "Позиция", KeyEvent.VK_A, icons.get(IconType.DELETE), "Обновление позиции", this::showUpdatePossition);
        JMenuItem updateDepartmentMenuItem = createSubMenuItem(updateMenu, "Департамент", KeyEvent.VK_A, icons.get(IconType.DELETE), "Обновление департамента", this::showUpdateDepartments);
        JMenuItem updateZvanieMenuItem = createSubMenuItem(updateMenu, "Звание", KeyEvent.VK_A, icons.get(IconType.DELETE), "Обновление звания", this::showUpdateZvanie);
        JMenuItem updateAuditoryMenuItem = createSubMenuItem(updateMenu, "Аудитория", KeyEvent.VK_A, icons.get(IconType.DELETE), "Обновление аудитории", this::showUpdateAufitory);
        JMenuItem updateAutorMenuItem = createSubMenuItem(updateMenu, "Автор", KeyEvent.VK_A, icons.get(IconType.DELETE), "Обновление страны для автора", this::showUpdateAutor);
        JMenuItem updateRoleMenuItem = createSubMenuItem(updateMenu, "Роль", KeyEvent.VK_A, icons.get(IconType.DELETE), "Обновление роли", this::showUpdateRole);

        JMenuItem helpMenuItem = createSubMenuItem(helpMenu, "О программе", KeyEvent.VK_A, icons.get(IconType.DELETE), "Инфо о программе", this::helpEvent);

        JMenu showDepartmentsMenu = createMenuItem("Департаменты",  KeyEvent.VK_A);
        showMenu.add(showDepartmentsMenu);
        JMenuItem showAllDepartmentItem = createSubMenuItem(showDepartmentsMenu, "Все департамент", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать все департаменты", this::showAllDepartments);


        JMenu showPossitionsMenu = createMenuItem("Позиции",  KeyEvent.VK_A);
        showMenu.add(showPossitionsMenu);
        JMenuItem showAllPossitionsItem = createSubMenuItem(showPossitionsMenu, "Все позиции", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать все позиции", this::showAllPossitions);


        JMenu showZvaniesMenu = createMenuItem("Звания",  KeyEvent.VK_A);
        showMenu.add(showZvaniesMenu);
        JMenuItem showAllZvaniesItem = createSubMenuItem(showZvaniesMenu, "Все звания", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать всех звания", this::showAllZvaniya);


        JMenu showRolesMenu = createMenuItem("Роли",  KeyEvent.VK_A);
        showMenu.add(showRolesMenu);
        JMenuItem showAllRolesItem = createSubMenuItem(showRolesMenu, "Все роли", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать все звания", this::showAllRoles);


        JMenu showAuditoriesMenu = createMenuItem("Аудитории",  KeyEvent.VK_A);
        showMenu.add(showAuditoriesMenu);
        JMenuItem showAllAuditoriesItem = createSubMenuItem(showAuditoriesMenu, "Все аудитории", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать все виды аудиторий", this::showAllAuditories);


        JMenu showGanresMenu = createMenuItem("Жанры",  KeyEvent.VK_A);
        showMenu.add(showGanresMenu);
        JMenuItem showAllGanresItem = createSubMenuItem(showGanresMenu, "Все виды жанров", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать все виды жанров", this::showAllGanres);

        JMenu showAutorsMenu = createMenuItem("Авторы",  KeyEvent.VK_A);
        showMenu.add(showAutorsMenu);
        JMenuItem showAllAutorsItem = createSubMenuItem(showAutorsMenu, "Все авторы", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать всех авторов", this::showAllAutors);


        JMenu showWorkersMenu = createMenuItem("Работники",  KeyEvent.VK_A);
        showMenu.add(showWorkersMenu);
        JMenuItem showAllWorkersItem = createSubMenuItem(showWorkersMenu, "Все работники", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать все виды аудиторий", this::showAllWorkers);
        JMenuItem showAllWorkersByBirthYearItem = createSubMenuItem(showWorkersMenu, "Все работники по году рождения", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать всех работников по году рождения", this::showAllWorkersByBirthYear);
        JMenuItem showAllWorkersByDepartmentItem = createSubMenuItem(showWorkersMenu, "Все работники по департаменту", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать всех работников департаменту", this::showAllWorkersByDepartment);
        JMenuItem showAllWorkersBySex = createSubMenuItem(showWorkersMenu, "Все работники по полу", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать всех работников по выбранному полу", this::showAllWorkersBySex);
        JMenuItem showAllWorkersByChildrenCount = createSubMenuItem(showWorkersMenu, "Все работники по числу детей", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать всех работников по кол-ву детей", this::showAllWorkersByChildrenCount);
        JMenuItem showAllWorkersBySalary = createSubMenuItem(showWorkersMenu, "Все работники по размеру зарплаты", KeyEvent.VK_A, icons.get(IconType.DELETE), "Показать всех работников по зарплате", this::showAllWorkersBySalary);


        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);

        Image finalBackground = background;
        mainPanel = new MainPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(finalBackground, 0, 0, this);
            }
        };
        scrollPane = new JScrollPane(mainPanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(5);
        scrollPane.setBackground(Color.WHITE);
        add(scrollPane);
        GridBagConstraints gbc1 = new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0);
        gbl.addLayoutComponent(scrollPane, gbc1);


        add(statusBar);
        Insets sBarInsets = new Insets(0, 0, 0, 0);
        GridBagConstraints sBarConstraints = new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, sBarInsets, 0, 0);
        gbl.addLayoutComponent(statusBar, sBarConstraints);

        setVisible(true);
    }


    private JMenu createMenuItem(String name, int mnemonic){
        JMenu menu = new JMenu(name);
        menu.setMnemonic(mnemonic);
        menuBar.add(menu);
        return menu;
    }

    private JMenuItem createSubMenuItem(JMenu menu, String name, int mnemonic, ImageIcon icon, String text, Runnable event){
        JMenuItem item = new JMenuItem(name, icon);
        item.setMnemonic(mnemonic);
        menu.add(item);
        statusBar.addHint(text, item);

        item.addActionListener(l -> {
            statusBar.showHint(null);
            if (event != null){
                event.run();
            }
        });

        item.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                statusBar.showHint(item);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                statusBar.showHint(null);
            }
        });
        activeComponents.add(item);
        item.setBorder(BorderFactory.createEmptyBorder());
        return item;
    }

    //*****************ДОБАВЛЕНИЕ*************************
    private void showAddDepartmentDialog(){
        new OneValueDialog(this, new Department("base"), departmentService, ActionType.SAVE, Constants.DEPARTMENT_TEXT);
    }

    private void showAddPossitionDialog(){
        new OneValueDialog(this, new Possition("base"), possitionService,ActionType.SAVE, Constants.POSSITION_TEXT);
    }

    private void showAddZvanieDialog(){
        new OneValueDialog(this, new Zvanie("base"), zvanieService, ActionType.SAVE,Constants.ZVANIE_TEXT);
    }

    private void showAddGenreDialog(){
        new OneValueDialog(this, new Ganre("base"), ganreService, ActionType.SAVE,Constants.GENRE_TEXT);
    }

    private void showAddRoleDialog(){
        new OneValueDialog(this, new Role("base"), roleService, ActionType.SAVE, Constants.ROLE_TEXT);
    }

    private void showAddWorkerDialog(){
        JTextField sexField = new JTextField("true/false");
        JTextField birthDateField = new JTextField("yyyy-mm-dd");
        JTextField offerDateField = new JTextField("yyyy-mm-dd");
        JTextField childrensField = new JTextField("childrensNumber");
        JTextField salaryField = new JTextField("salary in RUB");
        JTextField possitionIdField = new JTextField("possitionId");
        JTextField zvanieIdField = new JTextField("zvanieId");
        JTextField departmentIdField = new JTextField("departmentId");

        JPanel tmpPanel = new JPanel();
        tmpPanel.add(new Label("sex:"));
        tmpPanel.add(sexField);
        tmpPanel.add(new Label("birthDate:"));
        tmpPanel.add(birthDateField);
        tmpPanel.add(new Label("offerDate:"));
        tmpPanel.add(offerDateField);
        tmpPanel.add(new Label("childrens:"));
        tmpPanel.add(childrensField);
        tmpPanel.add(new Label("salary:"));
        tmpPanel.add(salaryField);
        tmpPanel.add(new Label("possition:"));
        tmpPanel.add(possitionIdField);
        tmpPanel.add(new Label("zvanie:"));
        tmpPanel.add(zvanieIdField);
        tmpPanel.add(new Label("department:"));
        tmpPanel.add(departmentIdField);
        tmpPanel.setSize(new Dimension(1180, 200));
        int res = JOptionPane.showConfirmDialog(this, tmpPanel, "Enter worker data", JOptionPane.YES_NO_OPTION);
        if (res == JOptionPane.OK_OPTION){
            Department department = departmentService.findById(Integer.parseInt(departmentIdField.getText()));
            System.out.println("departmentId vveli [" + Integer.parseInt(departmentIdField.getText()) + "]");
            if (department == null){
                JOptionPane.showMessageDialog(this, "Вы ввели несуществующий департамент, повторите позже");
                return;
            }
            Possition possition = possitionService.findById(Integer.parseInt(possitionIdField.getText()));
            System.out.println("possitionId vveli [" + Integer.parseInt(possitionIdField.getText()) + "]");
            if (possition == null){
                JOptionPane.showMessageDialog(this, "Вы ввели несуществующую позицию, повторите позже");
                return;
            }
            Zvanie zvanie = zvanieService.findById(19);
            //System.out.println("zvanieId vveli [" + Integer.parseInt(zvanieIdField.getText()) + "]");
            if (zvanie == null){
                JOptionPane.showMessageDialog(this, "Вы ввели несуществующее звание, повторите позже");
                return;
            }
            Worker worker = new Worker(Boolean.valueOf(sexField.getText()).booleanValue(), birthDateField.getText(), Integer.parseInt(salaryField.getText()),
                    offerDateField.getText(), possition, department, zvanie, Integer.parseInt(childrensField.getText()));
            workerService.add(worker);
        }
    }

    private void showAddAutorDialog(){
        JTextField fioField = new JTextField("<fio>");
        JTextField countryField = new JTextField("<country>");
        JTextField vekField = new JTextField("<vek>");

        JPanel tmpPanel = new JPanel();
        tmpPanel.add(new Label("fio:"));
        tmpPanel.add(fioField);
        tmpPanel.add(new Label("country:"));
        tmpPanel.add(countryField);
        tmpPanel.add(new Label("vek:"));
        tmpPanel.add(vekField);
//        tmpPanel.setSize(new Dimension(1180, 200));

        int res = JOptionPane.showConfirmDialog(this, tmpPanel, "Enter author data", JOptionPane.YES_NO_OPTION);
        if (res == JOptionPane.OK_OPTION){
            Autor autor = new Autor(fioField.getText(), countryField.getText(), Integer.parseInt(vekField.getText()));
            autorService.add(autor);
        }
    }

    //*****************УДАЛЕНИЕ***************************
    private void showDeleteDepartmentDialog(){
        new OneValueDialog(this, null, departmentService, ActionType.DELETE, Constants.DEPARTMENT_TEXT);
    }

    private void showDeletePossitionDialog(){
        new OneValueDialog(this, null, possitionService, ActionType.DELETE, Constants.POSSITION_TEXT);
    }

    private void showDeleteZvanieDialog(){
        new OneValueDialog(this, new Department(), zvanieService, ActionType.DELETE, Constants.ZVANIE_TEXT);
    }

    private void showDeleteGanreDialog(){
        new OneValueDialog(this, new Department(), ganreService, ActionType.DELETE, Constants.GENRE_TEXT);
    }

    private void showDeleteRoleDialog(){
        new OneValueDialog(this, null, roleService, ActionType.DELETE, Constants.ROLE_TEXT);
    }

    private void showDeleteWorkerDialog(){
        new OneValueDialog(this, null, workerService, ActionType.DELETE, Constants.WORKERS_TEXT);
    }

    private void showDeleteAutorDialog(){
        new OneValueDialog(this, null, autorService, ActionType.DELETE, Constants.AUTOR_TEXT);
    }



    //*******************ПРОСМОТР***********************

        //*******************ДЕПАРТАМЕНТЫ********************
        private void showAllDepartments(){
            StringBuilder stringBuilder = new StringBuilder();
            List<Department> departments = departmentService.findAll();
            for (Department tmp : departments){
                stringBuilder.append("id = " + tmp.getId() + " name = " + tmp.getName());
                stringBuilder.append("\n");
            }
            JOptionPane.showMessageDialog(this, stringBuilder.toString(), "Все департаменты", JOptionPane.PLAIN_MESSAGE);
        }

        //*******************ПОЗИЦИИ********************
        private void showAllPossitions(){
            StringBuilder stringBuilder = new StringBuilder();
            List<Possition> possitions = possitionService.findAll();
            for (Possition tmp : possitions){
                stringBuilder.append("id = " + tmp.getId() + " name = " + tmp.getName());
                stringBuilder.append("\n");
            }
            JOptionPane.showMessageDialog(this, stringBuilder.toString(), "Все позиции", JOptionPane.PLAIN_MESSAGE);
        }

        //*******************ЗВАНИЯ********************
        private void showAllZvaniya(){
            StringBuilder stringBuilder = new StringBuilder();
            List<Zvanie> zvanies = zvanieService.findAll();
            for (Zvanie tmp : zvanies){
                stringBuilder.append("id = " + tmp.getId() + " name = " + tmp.getName());
                stringBuilder.append("\n");
            }
            JOptionPane.showMessageDialog(this, stringBuilder.toString(), "Все звания", JOptionPane.PLAIN_MESSAGE);
        }

        //*******************РОЛИ********************
        private void showAllRoles(){
            StringBuilder stringBuilder = new StringBuilder();
            List<Role> roles = roleService.findAll();
            for (Role tmp : roles){
                stringBuilder.append("id = " + tmp.getId() + " name = " + tmp.getName());
                stringBuilder.append("\n");
            }
            JOptionPane.showMessageDialog(this, stringBuilder.toString(),  "Все роли", JOptionPane.PLAIN_MESSAGE);
        }

        //*******************АУДИТОРИИ********************
        private void showAllAuditories(){
            StringBuilder stringBuilder = new StringBuilder();
            List<Auditory> auditories = auditoryService.findAll();
            for (Auditory tmp : auditories){
                stringBuilder.append("id = " + tmp.getId() + " name = " + tmp.getName());
                stringBuilder.append("\n");
            }
            JOptionPane.showMessageDialog(this, stringBuilder.toString(),  "Все аудитории", JOptionPane.PLAIN_MESSAGE);
        }

        //*******************ЖАНРЫ************************
        private void showAllGanres(){
            StringBuilder stringBuilder = new StringBuilder();
            List<Ganre> ganres = ganreService.findAll();
            for (Ganre tmp : ganres){
                stringBuilder.append("id = " + tmp.getId() + " name = " + tmp.getName());
                stringBuilder.append("\n");
            }
            JOptionPane.showMessageDialog(this, stringBuilder.toString(),  "Все жанры", JOptionPane.PLAIN_MESSAGE);
        }

        //*******************Работники********************
        private void showAllWorkers(){
            StringBuilder stringBuilder = new StringBuilder();
            List<Worker> workers = workerService.findAll();
            Object message = getPreparedMessageToWorker(workers);
            JOptionPane.showMessageDialog(this, message,  "Все работники", JOptionPane.PLAIN_MESSAGE);
        }

        //*******************Авторы********************
        private void showAllAutors(){
            StringBuilder stringBuilder = new StringBuilder();
            List<Autor> autors = autorService.findAll();
            Object message = getPreparedMessageToAutor(autors);
            JOptionPane.showMessageDialog(this, message,  "Все авторы", JOptionPane.PLAIN_MESSAGE);
        }


        private void showAllWorkersByBirthYear() {
            String text = JOptionPane.showInputDialog("Введите год рождения");
            try{
                List<Worker> workers = workerService.findWorkersByBirthYear(Integer.parseInt(text));
                Object message = getPreparedMessageToWorker(workers);
                JOptionPane.showMessageDialog(this, message, "Все работники по году рождения", JOptionPane.PLAIN_MESSAGE);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(this, "Вы ввели некорректый год рождения");
            }
        }

        private void showAllWorkersByDepartment() {
            String text = JOptionPane.showInputDialog("Введите департамент");
            try{
                List<Worker> workers = workerService.findWorkersByDepartment(text);
                Object message = getPreparedMessageToWorker(workers);
                JOptionPane.showMessageDialog(this, message, "Все работники по году рождения", JOptionPane.PLAIN_MESSAGE);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(this, "Вы ввели некорректый год рождения");
            }
        }

        private void showAllWorkersBySex() {
            Object[] variants = {"true", "false"};
            boolean sex = Boolean.valueOf(String.valueOf(JOptionPane.showInputDialog(this, "Выберите пол", "М/Ж", JOptionPane.PLAIN_MESSAGE, null, variants, "true"))).booleanValue();
            try{
                List<Worker> workers = workerService.findWorkersBySex(sex);
                Object message = getPreparedMessageToWorker(workers);
                JOptionPane.showMessageDialog(this, message, "Все работники по полу", JOptionPane.PLAIN_MESSAGE);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(this, "Вы ввели некорректый пол");
            }
        }

        private void showAllWorkersByChildrenCount() {
            String text = JOptionPane.showInputDialog("Введите число детей");
            try{
                List<Worker> workers = workerService.findWorkersByChildrenCount(Integer.parseInt(text));
                Object message = getPreparedMessageToWorker(workers);
                JOptionPane.showMessageDialog(this, message, "Все работники с заданным числом детей", JOptionPane.PLAIN_MESSAGE);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(this, "Вы ввели некорректое число детей");
            }
        }

        private void showAllWorkersBySalary() {
            String text = JOptionPane.showInputDialog("Введите зарплату");
            try{
                List<Worker> workers = workerService.findWorkersBySalary(Integer.parseInt(text));
                Object message = getPreparedMessageToWorker(workers);
                JOptionPane.showMessageDialog(this, message, "Все работники с зарплатой >= введенной вами", JOptionPane.PLAIN_MESSAGE);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(this, "Вы ввели некорректое число");
            }
        }


    //****************ОБНОВЛЕНИЕ****************

        //***************ЖАНР*******************
        private void showUpdateGanre(){
            String text = JOptionPane.showInputDialog("Введите индекс жанра для редактирования");
            Ganre ganre = ganreService.findById(Integer.parseInt(text));
            if (ganre == null){
                JOptionPane.showMessageDialog(this, "Такого индекса жанра не существует");
                return;
            }
            new OneValueDialog(this, ganre, ganreService, ActionType.UPDATE,Constants.GENRE_TEXT);
        }

        //***************ПОЗИЦИЯ******************
        private void showUpdatePossition(){
            String text = JOptionPane.showInputDialog("Введите индекс позиции для редактирования");
            Possition possition = possitionService.findById(Integer.parseInt(text));
            if (possition == null){
                JOptionPane.showMessageDialog(this, "Такого индекса позиции не существует");
                return;
            }
            new OneValueDialog(this, possition, possitionService, ActionType.UPDATE,Constants.POSSITION_TEXT);
        }

        //******************ДЕПАРТАМЕНТ*******************
        private void showUpdateDepartments(){
            String text = JOptionPane.showInputDialog("Введите индекс департамента для редактирования");
            Department department = departmentService.findById(Integer.parseInt(text));
            if (department == null){
                JOptionPane.showMessageDialog(this, "Такого индекса департамента не существует");
                return;
            }
            new OneValueDialog(this, department, departmentService, ActionType.UPDATE,Constants.DEPARTMENT_TEXT);
        }

        //*****************ЗВАНИЕ****************
        private void showUpdateZvanie(){
            String text = JOptionPane.showInputDialog("Введите индекс звания для редактирования");
            Zvanie zvanie = zvanieService.findById(Integer.parseInt(text));
            if (zvanie == null){
                JOptionPane.showMessageDialog(this, "Такого индекса звания не существует");
                return;
            }
            new OneValueDialog(this, zvanie, zvanieService, ActionType.UPDATE,Constants.ZVANIE_TEXT);
        }

        //*****************АУДИТОРИЯ****************
        private void showUpdateAufitory(){
            String text = JOptionPane.showInputDialog("Введите индекс аудитории для редактирования");
            Auditory auditory = auditoryService.findById(Integer.parseInt(text));
            if (auditory == null){
                JOptionPane.showMessageDialog(this, "Такого индекса аудитории не существует");
                return;
            }
            new OneValueDialog(this, auditory, auditoryService, ActionType.UPDATE,Constants.AUDITORIES_TEXT);
        }

        //*****************РОЛЬ****************
        private void showUpdateRole(){
            String text = JOptionPane.showInputDialog("Введите индекс роли для редактирования");
            Role role = roleService.findById(Integer.parseInt(text));
            if (role == null){
                JOptionPane.showMessageDialog(this, "Такого индекса роли не существует");
                return;
            }
            new OneValueDialog(this, role, roleService, ActionType.UPDATE,Constants.ROLE_TEXT);
        }

        //*****************АВТОР****************
        private void showUpdateAutor(){
            String text = JOptionPane.showInputDialog("Введите индекс автора для редактирования");
            Autor autor = autorService.findById(Integer.parseInt(text));
            if (autor == null){
                JOptionPane.showMessageDialog(this, "Такого индекса автора не существует");
                return;
            }
            new OneValueDialog(this, autor, autorService, ActionType.UPDATE,Constants.AUTOR_TEXT);
        }


        private void helpEvent(){
        JOptionPane.showMessageDialog(this, Constants.HELP_TEXT);
    }

    private Object getPreparedMessageToWorker(List<Worker> workers){
        StringBuilder stringBuilder = new StringBuilder();
        Object message;
        for(Worker worker : workers){
            Possition possition = worker.getPossition();
            Department department = worker.getDepartment();
            Zvanie zvanie = worker.getZvanie();
            stringBuilder.append("id = " + worker.getId() + " sex = " + String.valueOf(worker.getSex()) + " birthDate = " + worker.getBirthdate().trim() +
                    " offerDate = " + worker.getOfferdate().trim() + " salary = " + worker.getSalary() + " possition = " + possition.getName().trim() +
                    " department = " + department.getName().trim() + " zvanie = " + zvanie.getName().trim() + " childrens = " + worker.getChildrens());
            stringBuilder.append("\n");
        }
        JScrollPane scrollPane = new JScrollPane(new Label("1"));
        scrollPane.setPreferredSize(new Dimension(1180, 250));
        JTextArea textArea = new JTextArea(stringBuilder.toString());
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setMargin(new Insets(2,2,2,2));
        scrollPane.getViewport().setView(textArea);
        message = scrollPane;
        return message;
    }

    private Object getPreparedMessageToAutor(List<Autor> autors){
        StringBuilder stringBuilder = new StringBuilder();
        Object message;
        for(Autor autor : autors){
            String country = autor.getCountry().trim();
            String fio = autor.getFio().trim();
            int vek = autor.getVek();
            stringBuilder.append("id = " + autor.getId() + " fio = " + fio + " country = " + country +
                    " vek = " + String.valueOf(vek));
            stringBuilder.append("\n");
        }
        JScrollPane scrollPane = new JScrollPane(new Label("1"));
        scrollPane.setPreferredSize(new Dimension(500, 200));
        JTextArea textArea = new JTextArea(stringBuilder.toString());
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setMargin(new Insets(2,2,2,2));
        scrollPane.getViewport().setView(textArea);
        message = scrollPane;
        return message;
    }

}
