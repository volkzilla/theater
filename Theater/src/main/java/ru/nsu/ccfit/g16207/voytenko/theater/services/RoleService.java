package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.RoleDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Role;

import java.util.List;

public class RoleService implements IService<Role> {
    private RoleDao dao = new RoleDao();

    @Override
    public void add(Role role) {
        dao.save(role);
    }

    @Override
    public void delete(Role role) {
        dao.delete(role);
    }

    @Override
    public void edit(Role role) {
        dao.update(role);
    }

    @Override
    public Role findById(int id) {
        return dao.findById(id);
    }

    @Override
    public List<Role> findAll() {
        return dao.findAll();
    }
}
