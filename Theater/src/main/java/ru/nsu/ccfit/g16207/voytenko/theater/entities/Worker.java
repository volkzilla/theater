package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import javax.persistence.*;

@Entity
@Table(name = "workers")
public class                                                                                            Worker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private boolean sex;
    private String birthdate;
    private int salary;
    private String offerdate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "possitionid")
    private Possition possition;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departmentid")
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "zvanieid")
    private Zvanie zvanie;

    private int childrens;

    public Worker(boolean sex, String birthdate, int salary, String offerdate, Possition possition,
                  Department department, Zvanie zvanie, int childrenCount){
        this.sex = sex;
        this.birthdate = birthdate;
        this.salary = salary;
        this.offerdate = offerdate;
        this.possition = possition;
        this.department = department;
        this.zvanie = zvanie;
        this.childrens = childrenCount;
    }

    public Worker() { }

    public int getId() {
        return id;
    }

    public boolean getSex(){
        return sex;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public int getSalary() {
        return salary;
    }

    public String getOfferdate() {
        return offerdate;
    }

    public Possition getPossition(){
        return possition;
    }

    public Department getDepartment() {
        return department;
    }

    public Zvanie getZvanie() {
        return zvanie;
    }

    public int getChildrens() {
        return childrens;
    }
}
