package ru.nsu.ccfit.g16207.voytenko.theater.services;

import java.util.List;

public interface IService<T> {
    void add(T t);

    void delete(T t);

    void edit(T t);

    T findById(int id);

    List<T> findAll();
}
