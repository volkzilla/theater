package ru.nsu.ccfit.g16207.voytenko.theater.entities;


import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.IdNameEntity;

import javax.persistence.*;

@Entity
@Table(name = "zvaniya")
public class Zvanie implements BaseEntity, IdNameEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    @Column(name = "name")
    private String name;

    public Zvanie(String name){
        this.name = name;
    }

    public Zvanie() { }

    @Override
    public void setText(String text) {
        this.name = text;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

}

