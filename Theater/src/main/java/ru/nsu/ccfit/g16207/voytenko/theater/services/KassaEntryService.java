package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.KassaEntryDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.KassaEntry;

import java.util.List;

public class KassaEntryService implements IService<KassaEntry> {
    private KassaEntryDao dao = new KassaEntryDao();

    @Override
    public void add(KassaEntry kassaEntry) {
        dao.save(kassaEntry);
    }

    @Override
    public void delete(KassaEntry kassaEntry) {
        dao.delete(kassaEntry);
    }

    @Override
    public void edit(KassaEntry kassaEntry) {
        dao.update(kassaEntry);
    }

    @Override
    public KassaEntry findById(int id) {
        return dao.findById(id);
    }

    @Override
    public List<KassaEntry> findAll() {
        return dao.findAll();
    }
}
