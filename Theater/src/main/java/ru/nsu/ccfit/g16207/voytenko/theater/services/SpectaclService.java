package ru.nsu.ccfit.g16207.voytenko.theater.services;

import ru.nsu.ccfit.g16207.voytenko.theater.dao.SpectaclDao;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.Spectacl;

import java.util.List;

public class SpectaclService implements IService<Spectacl> {
    private SpectaclDao spectaclDao = new SpectaclDao();

    @Override
    public void add(Spectacl spectacl) {
        spectaclDao.save(spectacl);
    }

    @Override
    public void delete(Spectacl spectacl) {
        spectaclDao.delete(spectacl);
    }

    @Override
    public void edit(Spectacl spectacl) {
        spectaclDao.update(spectacl);
    }

    @Override
    public Spectacl findById(int id) {
        return spectaclDao.findById(id);
    }

    @Override
    public List<Spectacl> findAll() {
        return spectaclDao.findAll();
    }
}
