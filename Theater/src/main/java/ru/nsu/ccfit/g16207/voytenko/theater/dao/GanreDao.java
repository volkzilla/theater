package ru.nsu.ccfit.g16207.voytenko.theater.dao;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.Ganre;

public class GanreDao extends BaseDao<Ganre> {
    public GanreDao() {
        super(Ganre.class);
    }
}
