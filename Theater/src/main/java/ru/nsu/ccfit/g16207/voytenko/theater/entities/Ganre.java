package ru.nsu.ccfit.g16207.voytenko.theater.entities;

import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.BaseEntity;
import ru.nsu.ccfit.g16207.voytenko.theater.entities.interfaces.IdNameEntity;

import javax.persistence.*;

@Entity
@Table(name = "ganres")
public class Ganre implements BaseEntity, IdNameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 1;

    private String name;

    public Ganre(String name){
        this.name = name;
    }

    public Ganre() { }


    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public void setText(String text) {
        this.name = text;
    }
}
